# LODAtlas Commons
This project contains classes used by both [lodatlas-datamanager](https://gitlab.inria.fr/epietrig/lodatlas-datamanager) and [lodatlas-server](https://gitlab.inria.fr/epietrig/lodatlas-server) projects.

# License
LODAtlas Commons is licensed under [GNU General Public License version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)