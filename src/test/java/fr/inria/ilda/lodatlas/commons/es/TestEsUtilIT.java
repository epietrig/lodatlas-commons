/*******************************************************************************
 * LODAtlas Commons
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.commons.es;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.FileNotFoundException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.node.Node;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import fr.inria.ilda.lodatlas.commons.es.exceptions.MappingFileException;

/**
 * A test class to test {@link EsUtil} class.
 * 
 * @see EsUtil
 * 
 * @author Hande Gözükan
 *
 */
public class TestEsUtilIT {

	// this value should be the same as "elasticsearch.cluster.name" value in
	// pom.xml
	// put this in properties file
	String CLUSTER = "testlodatlas";
	String HOST = "localhost";
	// this value should be the same as "elasticsearch.transport.port" value in the
	// pom.xml
	int PORT = 9500;

	String INDEX = "testindex";
	// name of the root element in test_mapping.json file
	String TYPE = "testType";
	String ANALYZER_FILE = "test_analyzers.json";
	String MAPPING_FILE = "test_mapping.json";
	String MAPPING_FILE_ROOT_NO_MATCH = "test_mapping_root_no_match.json";
	String INVALID_MAPPING = "invalid_mapping.json";
	String NON_EXISTING = "nonexisting";

	String TEST_JSON = "{\"uri\":\"http://purl.org/dc/elements/1.1/\", \"title\": \"Dublin Core Metadata Element Set\", \"homepage\":\"http://dublincore.org/documents/dces\", \"description\":\"description\"}";
	String TEST_ID = "1";

	TransportClient client;
	Node node;

	EsUtil esUtil;

	@BeforeAll
	public void initAll() throws UnknownHostException {
		final Settings settings = Settings.builder().put(EsUtil.CLUSTER_NAME_KEY, CLUSTER).build();

		client = new PreBuiltTransportClient(settings);
		client.addTransportAddress(new TransportAddress(InetAddress.getByName(HOST), PORT));

		esUtil = new EsUtil(CLUSTER, PORT, HOST);
	}

	@BeforeEach
	public void init() {
		IndicesExistsResponse response = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		if (response.isExists()) {
			client.admin().indices().prepareDelete(INDEX).execute().actionGet();
		}
	}

	@AfterAll
	public void tearDownAll() {
		client = null;

		esUtil.dispose();
		esUtil = null;
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when no index with specified <code>indexName</code> exists.
	 * 
	 * @throws FileNotFoundException
	 * @throws MappingFileException
	 */
	@Test
	public void testCreateIndex1() throws FileNotFoundException, MappingFileException {
		// assert that index does not exist
		IndicesExistsResponse response = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertFalse(response.isExists());

		// create index and assert that it exists
		esUtil.createIndex(INDEX, TYPE, MAPPING_FILE, ANALYZER_FILE, false);
		response = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertTrue(response.isExists());
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when an index with specified <code>indexName</code> exists and
	 * <code>deleteIfExists</code> is set to <code>true</code>.
	 * 
	 * @throws FileNotFoundException
	 * @throws MappingFileException
	 */
	@Test
	public void testCreateIndex2() throws FileNotFoundException, MappingFileException {
		// create index
		CreateIndexResponse createIndexResponse = client.admin().indices().create(new CreateIndexRequest(INDEX))
				.actionGet();
		assertTrue(createIndexResponse.isAcknowledged());

		// assert that the index exists
		IndicesExistsResponse indexExistsResponse = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertTrue(indexExistsResponse.isExists());

		// add document to index
		client.prepareIndex(INDEX, TYPE, TEST_ID).setSource(TEST_JSON, XContentType.JSON).get();

		// assert that the document exists in the index
		GetResponse response = client.prepareGet(INDEX, TYPE, "1").get();
		assertTrue(response.isExists());

		// create index setting deleteIf exists set to true
		esUtil.createIndex(INDEX, TYPE, MAPPING_FILE, ANALYZER_FILE, true);
		indexExistsResponse = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertTrue(indexExistsResponse.isExists());

		// refresh indices
		client.admin().indices().prepareRefresh().execute().actionGet();

		// assert that no document exists in the created index
		SearchResponse sr = client.prepareSearch(INDEX).setTypes(TYPE).setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.get();
		assertEquals(0, sr.getHits().totalHits);
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when an index with specified <code>indexName</code> exists and
	 * <code>deleteIfExists</code> is set to <code>false</code>.
	 * 
	 * @throws FileNotFoundException
	 * @throws MappingFileException
	 */
	@Test
	public void testCreateIndex3() throws FileNotFoundException, MappingFileException {
		// create index
		CreateIndexResponse createIndexResponse = client.admin().indices().create(new CreateIndexRequest(INDEX))
				.actionGet();
		assertTrue(createIndexResponse.isAcknowledged());

		// assert that the index exists
		IndicesExistsResponse indexExistsResponse = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertTrue(indexExistsResponse.isExists());

		// add document to index
		client.prepareIndex(INDEX, TYPE, TEST_ID).setSource(TEST_JSON, XContentType.JSON).get();

		// assert that the document exists in the index
		GetResponse response = client.prepareGet(INDEX, TYPE, "1").get();
		assertTrue(response.isExists());

		// create index setting deleteIf exists set to false
		esUtil.createIndex(INDEX, TYPE, MAPPING_FILE, ANALYZER_FILE, false);
		indexExistsResponse = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertTrue(indexExistsResponse.isExists());

		// assert that the document exists in the index
		response = client.prepareGet(INDEX, TYPE, "1").get();
		assertTrue(response.isExists());
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when <code>null</code> <code>mappingFile</code> is specified.
	 * 
	 * @throws FileNotFoundException
	 * @throws MappingFileException
	 */
	@Test
	public void testCreateIndex4() throws FileNotFoundException, MappingFileException {
		// assert that index does not exist
		IndicesExistsResponse response = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertFalse(response.isExists());

		// create index and assert that it exists
		esUtil.createIndex(INDEX, TYPE, null, ANALYZER_FILE, false);
		response = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertTrue(response.isExists());
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when <code>mappingFile</code> specified is empty string.
	 * 
	 * @throws FileNotFoundException
	 * @throws MappingFileException
	 */
	@Test
	public void testCreateIndex5() throws FileNotFoundException, MappingFileException {
		// assert that index does not exist
		IndicesExistsResponse response = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertFalse(response.isExists());

		// create index and assert that it exists
		esUtil.createIndex(INDEX, TYPE, "", ANALYZER_FILE, false);
		response = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertTrue(response.isExists());
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when <code>null</code> <code>analyzersFile</code> is specified.
	 * 
	 * @throws FileNotFoundException
	 * @throws MappingFileException
	 */
	@Test
	public void testCreateIndex6() throws FileNotFoundException, MappingFileException {
		// assert that index does not exist
		IndicesExistsResponse response = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertFalse(response.isExists());

		// create index and assert that it exists
		esUtil.createIndex(INDEX, TYPE, MAPPING_FILE, ANALYZER_FILE, false);
		response = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertTrue(response.isExists());
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when <code>anayzersFile</code> specified is empty string.
	 * 
	 * @throws FileNotFoundException
	 * @throws MappingFileException
	 */
	@Test
	public void testCreateIndex7() throws FileNotFoundException, MappingFileException {
		// assert that index does not exist
		IndicesExistsResponse response = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertFalse(response.isExists());

		// create index and assert that it exists
		esUtil.createIndex(INDEX, TYPE, MAPPING_FILE, ANALYZER_FILE, false);
		response = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertTrue(response.isExists());
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when the specified <code>indexName</code> is <code>null</code>.
	 */
	@Test
	void testCreateIndex_Ex1() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			esUtil.createIndex(null, TYPE, MAPPING_FILE, ANALYZER_FILE, true);
		});
		assertEquals("Index name cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when the specified <code>indexName</code> is empty string.
	 */
	@Test
	void testCreateIndex_Ex2() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			esUtil.createIndex("", TYPE, MAPPING_FILE, ANALYZER_FILE, true);
		});
		assertEquals("Index name cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when the specified <code>mappingType</code> is <code>null</code>.
	 */
	@Test
	void testCreateIndex_Ex3() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			esUtil.createIndex(INDEX, null, MAPPING_FILE, ANALYZER_FILE, true);
		});
		assertEquals("Mapping type cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when the specified <code>mappingType</code> is empty string.
	 */
	@Test
	void testCreateIndex_Ex4() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			esUtil.createIndex(INDEX, "", MAPPING_FILE, ANALYZER_FILE, true);
		});
		assertEquals("Mapping type cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when the specified <code>mappingFile</code> does not exist on classpath.
	 */
	@Test
	void testCreateIndex_Ex5() {
		FileNotFoundException ex = assertThrows(FileNotFoundException.class, () -> {
			esUtil.createIndex(INDEX, TYPE, NON_EXISTING, ANALYZER_FILE, true);
		});
		assertEquals("Mapping file does not exist on classpath.", ex.getMessage());
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when the specified <code>mappingFile</code> is malformed.
	 */
	@Test
	@Disabled
	void testCreateIndex_Ex6() {
		// TODO implement
		fail();
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when the specified <code>mappingFile</code> does not have matching fields.
	 * 
	 * @throws FileNotFoundException
	 */
	@Test
	void testCreateIndex_Ex7() throws FileNotFoundException, MappingFileException {
		MappingFileException ex = assertThrows(MappingFileException.class, () -> {
			esUtil.createIndex(INDEX, TYPE, MAPPING_FILE_ROOT_NO_MATCH, ANALYZER_FILE, true);
		});
		assertEquals("Problem with mapping file.", ex.getMessage());
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when the specified <code>analyzersFile</code> does not exist on classpath.
	 */
	@Test
	void testCreateIndex_Ex8() {
		FileNotFoundException ex = assertThrows(FileNotFoundException.class, () -> {
			esUtil.createIndex(INDEX, TYPE, MAPPING_FILE, NON_EXISTING, true);
		});
		assertEquals("Analyzers file does not exist on classpath.", ex.getMessage());
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when the specified <code>analyzersFile</code> is malformed.
	 */
	@Test
	@Disabled
	void testCreateIndex_Ex9() {
		// TODO implement
		fail();
	}

	/**
	 * Tests {@link EsUtil#createIndex(String, String, String, String, boolean)}
	 * when the specified <code>analyzersFile</code> does not have matching fields.
	 */
	@Test
	@Disabled
	void testCreateIndex_Ex10() {
		// TODO implement
		fail();
	}

	@Test
	public void testIndexDocument() {
		// create index
		CreateIndexResponse createIndexResponse = client.admin().indices().create(new CreateIndexRequest(INDEX))
				.actionGet();
		assertTrue(createIndexResponse.isAcknowledged());

		// assert that the index exists
		IndicesExistsResponse indexExistsResponse = client.admin().indices().prepareExists(INDEX).execute().actionGet();
		assertTrue(indexExistsResponse.isExists());

		// add document to index
		esUtil.indexDocument(INDEX, TYPE, TEST_ID, TEST_JSON);

		// assert that the document exists in the index
		GetResponse response = client.prepareGet(INDEX, TYPE, TEST_ID).get();
		assertTrue(response.isExists());
	}

	/**
	 * Tests {@link EsUtil#indexDocument(String, String, String, String)} when the
	 * specified <code>indexName</code> is <code>null</code>.
	 */
	@Test
	void testIndexDocument_Ex1() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			esUtil.indexDocument(null, TYPE, TEST_ID, TEST_JSON);
		});
		assertEquals("Index name cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link EsUtil#indexDocument(String, String, String, String)} when the
	 * specified <code>indexName</code> is empty string.
	 */
	@Test
	void testIndexDocument_Ex2() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			esUtil.indexDocument("", TYPE, TEST_ID, TEST_JSON);
		});
		assertEquals("Index name cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link EsUtil#indexDocument(String, String, String, String)} when the
	 * specified <code>mappingType</code> is <code>null</code>.
	 */
	@Test
	void testIndexDocument_Ex3() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			esUtil.indexDocument(INDEX, null, TEST_ID, TEST_JSON);
		});
		assertEquals("Mapping type cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link EsUtil#indexDocument(String, String, String, String)} when the
	 * specified <code>mappingType</code> is empty string.
	 */
	@Test
	void testIndexDocument_Ex4() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			esUtil.indexDocument(INDEX, "", TEST_ID, TEST_JSON);
		});
		assertEquals("Mapping type cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link EsUtil#indexDocument(String, String, String, String)} when the
	 * specified <code>json</code> is <code>null</code>.
	 */
	@Test
	void testIndexDocument_Ex5() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			esUtil.indexDocument(INDEX, TYPE, TEST_ID, null);
		});
		assertEquals("Document to index cannot be null or empty string.", ex.getMessage());
	}

	/**
	 * Tests {@link EsUtil#indexDocument(String, String, String, String)} when the
	 * specified <code>json</code> is empty string.
	 */
	@Test
	void testIndexDocument_Ex6() {
		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			esUtil.indexDocument(INDEX, TYPE, TEST_ID, "");
		});
		assertEquals("Document to index cannot be null or empty string.", ex.getMessage());
	}

}
