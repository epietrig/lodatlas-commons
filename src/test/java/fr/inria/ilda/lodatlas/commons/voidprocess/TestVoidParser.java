package fr.inria.ilda.lodatlas.commons.voidprocess;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.inria.ilda.lodatlas.commons.model.FormatType;
import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.commons.strings.ResourceStrings;
import fr.inria.ilda.lodatlas.commons.voidprocess.VoidParser;

/**
 * A test class to test {@link VoidParser} class.
 * 
 * @see VoidParser
 * 
 * @author Hande Gözükan
 *
 */
class TestVoidParser {

	VoidParser parser;

	String TEST_DATASET = "test";

	String INVALID_URI = "http://";
	String EMPTY_URI = "";
	String LOCAL_URI_WITHOUT_X = "test_files/b3kat";
	String URI_WITHOUT_X = "http://lod.b3kat.de/.well-known/void";

	// A VoID file that has 4 linksets defined
	String MODEL_LODBID = "test_files/lobid.ttl";
	Model model_LODBID;

	String MODEL_ThIST = "test_files/ThISTVoid.ttl";
	Model model_ThIST;

	String MODEL_ASSOCIATIONS = "test_files/associations.ttl";
	Model model_Associations;

	@BeforeEach
	void setUp() throws Exception {
		parser = new VoidParser();

		// Load a VoID dataset that contains 4 linksets
		model_LODBID = ModelFactory.createDefaultModel().read(MODEL_LODBID);
		model_ThIST = ModelFactory.createDefaultModel().read(MODEL_ThIST);
		model_Associations = ModelFactory.createDefaultModel().read(MODEL_ASSOCIATIONS);
	}

	@AfterEach
	void tearDown() throws Exception {
		parser = null;
		model_LODBID = null;
		model_ThIST = null;
		model_Associations = null;
	}

	/**
	 * Tests {@link VoidParser#readModel(String)} for empty URI.
	 */
	@Test
	void testReadModel_1() {
		assertNull(parser.readModel(EMPTY_URI));
	}

	/**
	 * Tests {@link VoidParser#readModel(String)} for invalid URI.
	 */
	@Test
	void testReadModel_2() {
		assertNull(parser.readModel(INVALID_URI));
	}

	/**
	 * Tests {@link VoidParser#readModel(String)} for null URI.
	 */
	@Test
	void testReadModel_3() {
		assertNull(parser.readModel(null));
	}

	/**
	 * Tests {@link VoidParser#readModel(String)} for valid URI without extension
	 * where content type can be obtained.
	 */
	@Test
	void testReadModel_4() {
		Model model = parser.readModel(URI_WITHOUT_X);
		assertNotNull(model);
		assertFalse(model.isEmpty());
	}

	/**
	 * Tests {@link VoidParser#readModel(String)} for valid local URI without
	 * extension.
	 */
	@Test
	void testReadModel_5() {
		Model model = parser.readModel(LOCAL_URI_WITHOUT_X);
		assertNull(model);
	}

	@Test
	void testProcessDatasetVoid() {
		JsonObject jsonObj = parser.processDatasetVoid(MODEL_LODBID);

		// Dataset is specified by void:Linkset void:subjectsTarget
		assertEquals("http://lobid.org/resource", jsonObj.getString(DatasetStrings.URL));

		// Dataset is specified with description
		jsonObj = parser.processDatasetVoid(MODEL_ThIST);
		assertEquals("http://www.isprambiente.gov.it/it/biblioteca/info/pubblicazioni-della-biblioteca/thist-1",
				jsonObj.getString(DatasetStrings.URL));

		// parser.processDatasetVoid("", URI_WITHOUT_X);
		//
		// parser.processDatasetVoid("", "http://lobid.org/dataset/resource/about.ttl");
		//
		// parser.processDatasetVoid("",
		// "https://raw.githubusercontent.com/frmichel/taxref-ld/master/10.0/Taxrefld10_void.ttl");
		//
		// parser.processDatasetVoid("", "https://w3id.org/associations/void.ttl");
		//
		// parser.processDatasetVoid("", "http://rdf.muninn-project.org/Muninn.rdf");
		//
		// parser.processDatasetVoid("", "http://dbpedia.org/void/Dataset");
	}

	/**
	 * Tests {@link VoidParser#processLinksets(Model, java.util.Map)} method with
	 * MODEL_LODBID which has 4 linksets defined.
	 */
	@Test
	void testProcessLinksets_1() {
		Map<String, Resource> linkset = new HashMap<>();
		JsonObjectBuilder datasetBuilder = Json.createObjectBuilder();

		RDFNode main = parser.processLinksets(model_LODBID, linkset, datasetBuilder);

		assertEquals(4, linkset.size());

		// could not manage to set base URI as desired, takes file path when it does
		// not exist in the file. Would give different results on different machines.

		// assertTrue(linkset.containsKey("http://lobid.org/dataset/resource/about.ttl#lobidRes-lobidOrg"));
		// assertTrue(linkset.containsKey("http://lobid.org/dataset/resource/about.ttl#lobidRes-GND"));
		// assertTrue(linkset.containsKey("http://lobid.org/dataset/resource/about.ttl#lobidRes-DDC"));
		// assertTrue(linkset.containsKey("http://lobid.org/dataset/resource/about.ttl#lobidRes-B3kat"));

		assertEquals("http://lobid.org/dataset/resource", main.asResource().toString());
	}

	/**
	 * Tests {@link VoidParser#processLinksets(Model, java.util.Map)} method with
	 * MODEL_ThIST which has 3 linksets defined.
	 */
	@Test
	void testProcessLinksets_2() {
		Map<String, Resource> linkset = new HashMap<>();
		JsonObjectBuilder datasetBuilder = Json.createObjectBuilder();

		RDFNode main = parser.processLinksets(model_ThIST, linkset, datasetBuilder);

		assertEquals(3, linkset.size());
		assertTrue(linkset.containsKey("http://purl.org/NET/ThISTVOID#ThIST-EARTh"));
		assertTrue(linkset.containsKey("http://purl.org/NET/ThISTVOID#EARTh-DBPEDIA"));
		assertTrue(linkset.containsKey("http://purl.org/NET/ThISTVOID#ThIST-GEMET"));

		assertEquals("http://purl.org/NET/ThIST", main.asResource().toString());
	}

	/**
	 * Tests {@link VoidParser#processLinksets(Model, java.util.Map)} method with
	 * MODEL_ASSOCIATIONS which has 1 linkset defined.
	 */
	@Test
	void testProcessLinksets_3() {
		Map<String, Resource> linkset = new HashMap<>();
		JsonObjectBuilder datasetBuilder = Json.createObjectBuilder();

		RDFNode main = parser.processLinksets(model_Associations, linkset, datasetBuilder);

		assertEquals(1, linkset.size());
	}

	/**
	 * Tests {@link VoidParser#processDatasetDesc(Model)} with MODEL_ThIST VoID
	 * file.
	 */
	@Test
	void testProcessDatasetDesc_1() {
		RDFNode mainDataset = parser.processDatasetDesc(model_ThIST);
		assertNotNull(mainDataset);
		assertEquals("http://purl.org/NET/ThIST", mainDataset.asResource().toString());
	}

	/**
	 * Tests {@link VoidParser#processDatasetDesc(Model)} with MODEL_ASSOCIATIONS
	 * VoID file.
	 */
	@Test
	void testProcessDatasetDesc_2() {
		RDFNode mainDataset = parser.processDatasetDesc(model_Associations);
		assertNotNull(mainDataset);
		assertEquals("https://w3id.org/associations/void.ttl#Associations", mainDataset.asResource().toString());
	}

	/**
	 * Tests {@link VoidParser#processMainDataset(RDFNode, JsonObjectBuilder)} with
	 * MODEL_LODBID VoID file.
	 */
	@Test
	void testProcessMainDataset_1() {
		JsonObjectBuilder datasetBuilder = Json.createObjectBuilder();
		RDFNode mainDataset = model_LODBID.getResource("http://lobid.org/dataset/resource");

		parser.processMainDataset(mainDataset, datasetBuilder);

		JsonObject jsonObj = datasetBuilder.build();

		assertEquals("http://lobid.org/resource", jsonObj.getString(DatasetStrings.URL));
		assertEquals("lobid. Bibliographic Resources", jsonObj.getString(DatasetStrings.TITLE));
		assertEquals(
				"Open Bibliographic Data in RDF under use of the Bibliographic Ontology. Gathered and published by the hbz. See https://wiki1.hbz-nrw.de/display/SEM/LOD+Mapping+201107",
				jsonObj.getString(DatasetStrings.NOTES));
		assertEquals("http://lobid.org/dataset/", jsonObj.getString(DatasetStrings.NAMESPACE));
		assertEquals("http://lobid.org/organisation/DE-605",
				jsonObj.getJsonObject(DatasetStrings.ORGANIZATION).getString(DatasetStrings.TITLE));
		assertEquals("2010-08-31", jsonObj.getString(DatasetStrings.METADATA_CREATED));
		assertEquals("2012-03", jsonObj.getString(DatasetStrings.METADATA_MODIFIED));
		assertEquals("http://creativecommons.org/publicdomain/zero/1.0/",
				jsonObj.getString(DatasetStrings.LICENSE_URL));

		JsonArray resourceList = jsonObj.getJsonArray(DatasetStrings.RESOURCES);

		// check sparql endpoint
		assertEquals("http://lobid.org/sparql/", resourceList.get(0).asJsonObject().getString(ResourceStrings.URL));
		assertEquals(FormatType.SPARQL.value(), resourceList.get(0).asJsonObject().getString(ResourceStrings.FORMAT));

		// check example resource
		assertEquals("http://lobid.org/resource/HT002948556",
				resourceList.get(1).asJsonObject().getString(ResourceStrings.URL));

		// check dump resource
		assertEquals("http://lobid.org/download/dumps/hbzlod.ttl.tar.bz2",
				resourceList.get(2).asJsonObject().getString(ResourceStrings.URL));
		// assertEquals(12,
		// resourceList.get(2).asJsonObject().getJsonArray(ProcessedFileStrings.VOCABULARIES).size());

		// check size of resources
		assertEquals(3, resourceList.size());

		// check tags
		assertEquals(1, jsonObj.getJsonArray(DatasetStrings.TAGS).size());
		assertEquals("Literature", jsonObj.getJsonArray(DatasetStrings.TAGS).getString(0));
	}

	/**
	 * Tests {@link VoidParser#processMainDataset(RDFNode, JsonObjectBuilder)} with
	 * MODEL_ThIST VoID file.
	 */
	@Test
	void testProcessMainDataset_2() {
		JsonObjectBuilder datasetBuilder = Json.createObjectBuilder();
		RDFNode mainDataset = model_ThIST.getResource("http://purl.org/NET/ThIST");

		parser.processMainDataset(mainDataset, datasetBuilder);
		JsonObject jsonObj = datasetBuilder.build();

		assertEquals("http://www.isprambiente.gov.it/it/biblioteca/info/pubblicazioni-della-biblioteca/thist-1",
				jsonObj.getString(DatasetStrings.URL));
		assertEquals("ThIST", jsonObj.getString(DatasetStrings.TITLE));
		assertEquals(
				"ThIST is the Italian Thesaurus of Sciences of the Earth, which has been exposed as Linked Data in the context of LusTRE, a framework currently under development within the EU project eENVplus (CIP-ICT-PSP grant No. 325232) that aims at combining existing thesauri to support the management of environmental resources. LusTRE considers the heterogeneity in scopes and levels of abstraction of environmental thesauri as an asset when managing environmental data, it exploits linked data best practices SKOS (Simple Knowledge Organization System) and RDF (Resource Description Framework) in order to provide a multi-thesauri solution for INSPIRE data themes related to the environment. The ThIST content is made available by Geological Survey of Italy in ISPRA (Istituto Superiore per la Protezione e la Ricerca Ambientale). It is the result of a nearly total revision of the thesaurus born by the integration among the terminological database of the former Library of the Italy Geological Service and the thesaurus published by the CNR in 1997. ThIST is already used for the cataloguing of the biblio-cartographic material (monographs, articles of periodicals, papers) possessed by the Library and the recovery of the related information. The ThIST content is made available by ISPRA (Istituto Superiore per la Protezione e la Ricerca Ambientale). It is the result of a nearly total revision of the thesaurus born by the integration among the terminological database of the former Library of the Italy Geological Service and the thesaurus published by the CNR in 1997. ThIST is already used for the cataloguing of the biblio-cartographic material (monographs, articles of periodicals, papers) possessed by the Library and the recovery of the related information.",
				jsonObj.getString(DatasetStrings.NOTES));
		assertEquals("http://purl.org/NET/", jsonObj.getString(DatasetStrings.NAMESPACE));
		assertEquals("http://purl.org/NET/ThISTVOID#ISPRA", jsonObj.getString(DatasetStrings.AUTHOR));
		assertEquals("http://dblp.L3S.de/Authors/Monica_De_Martino",
				jsonObj.getJsonObject(DatasetStrings.ORGANIZATION).getString(DatasetStrings.TITLE));
		assertEquals(null, jsonObj.get(DatasetStrings.METADATA_CREATED));
		assertEquals("2014-08-04", jsonObj.getString(DatasetStrings.METADATA_MODIFIED));
		assertEquals("http://opendefinition.org/licenses/cc-by/", jsonObj.getString(DatasetStrings.LICENSE_URL));

		JsonArray resourceList = jsonObj.getJsonArray(DatasetStrings.RESOURCES);

		// check sparql endpoint
		assertEquals("http://linkeddata.ge.imati.cnr.it:8890/sparql",
				resourceList.get(0).asJsonObject().getString(ResourceStrings.URL));
		assertEquals(FormatType.SPARQL.value(), resourceList.get(0).asJsonObject().getString(ResourceStrings.FORMAT));

		// check example resource
		assertEquals("http://linkeddata.ge.imati.cnr.it/resource/ThIST/affioramento",
				resourceList.get(1).asJsonObject().getString(ResourceStrings.URL));

		// check dump resource
		assertEquals("http://purl.org/net/DumpThISTRDF",
				resourceList.get(2).asJsonObject().getString(ResourceStrings.URL));
		// assertEquals(2,
		// resourceList.get(2).asJsonObject().getJsonArray(ProcessedFileStrings.VOCABULARIES).size());

		// check size of resources
		assertEquals(3, resourceList.size());

		// check tags
		assertEquals(2, jsonObj.getJsonArray(DatasetStrings.TAGS).size());
		assertEquals("Geology", jsonObj.getJsonArray(DatasetStrings.TAGS).getString(0));
		assertEquals("Thesaurus", jsonObj.getJsonArray(DatasetStrings.TAGS).getString(1));
	}

	/**
	 * Tests {@link VoidParser#processMainDataset(RDFNode, JsonObjectBuilder)} with
	 * MODEL_ThIST VoID file.
	 */
	@Test
	void testProcessMainDataset_3() {
		JsonObjectBuilder datasetBuilder = Json.createObjectBuilder();
		RDFNode mainDataset = model_Associations.getResource("https://w3id.org/associations/void.ttl#Associations");

		parser.processMainDataset(mainDataset, datasetBuilder);
		JsonObject jsonObj = datasetBuilder.build();

		assertEquals("https://w3id.org/associations", jsonObj.getString(DatasetStrings.URL));
		assertEquals("Human Associations", jsonObj.getString(DatasetStrings.TITLE));
		assertEquals("Human Association datasets as Linked Data", jsonObj.getString(DatasetStrings.NOTES));
		assertEquals("https://w3id.org/associations/void.ttl#", jsonObj.getString(DatasetStrings.NAMESPACE));
		assertEquals(null, jsonObj.get(DatasetStrings.METADATA_CREATED));
		assertEquals("2017-06-16", jsonObj.getString(DatasetStrings.METADATA_MODIFIED));
	}

}
