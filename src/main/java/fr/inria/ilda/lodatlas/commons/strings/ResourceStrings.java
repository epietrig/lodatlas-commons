/*******************************************************************************
 * LODAtlas Commons
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.commons.strings;

public class ResourceStrings {

	/**
	 * Field name for resource id for a dataset resource.
	 */
	public static final String ID = "id";

	/**
	 * Field name for resource URL for a dataset resource.
	 */
	public static final String URL = "url";

	public static final String NAME = "name";

	public static final String DESCRIPTION = "description";

	public static final String FORMAT = "format";

	public static final String VERIFIED_FORMAT = "verified_format";

	public static final String CAN_DOWNLOAD = "can_download";

	public static final String IS_COMPRESSED = "is_compressed";

	public static final String DOWNLOAD_ERROR = "download_error";

	public static final String PROCESSED_FILES = "processed_files";

	public static final String STATE = "state";

	public static final String CONTENT_TYPE = "content_type";

	public static final String LAST_MODIFIED = "last_modified";

	public static final String DUMP_FILENAME = "dump_file_name";
	
	public static final String PROCESSED = "processed";

}
