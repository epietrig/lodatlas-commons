package fr.inria.ilda.lodatlas.commons.strings;

public class VocabularyStrings {

	public static final String URI = "uri";

	public static final String LOV_URI = "lovUri";

	public static final String TITLE = "title";

	public static final String PREFIX = "prefix";

	public static final String HOMEPAGE = "homepage";

	public static final String DESCRIPTION = "description";

	public static final String CLASSES = "classes";

	public static final String PROPERTIES = "properties";

	public static final String LABEL = "label";

}
