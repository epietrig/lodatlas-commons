/*******************************************************************************
 * LODAtlas Commons
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.commons.strings;

/**
 * Strings that appear in mapping_processed_file.json file.
 * 
 * @author Hande Gözükan
 *
 */
public class ProcessedFileStrings {

	public static final String REPO_NAME = "repoName";

	public static final String DATASET_NAME = "datasetName";

	public static final String RESOURCE_ID = "resourceId";

	public static final String FILE_NAME = "fileName";

	public static final String ORIGINAL_FILE_NAME = "originalFileName";

	public static final String TRIPLE_COUNT = "tripleCount";

	public static final String HAS_ERROR = "hasError";

	public static final String ERROR = "error";

	public static final String USED_CLASSES = "usedClasses";
	
	public static final String CLASS_LABELS = "classLabels";

	public static final String USED_PROPERTIES = "usedProperties";
	
	public static final String PROPERTY_LABELS = "propertyLabels";

	public static final String VOCABULARIES = "vocabularies";

	public static final String FD = "fd";

	public static final String HEB = "heb";

	public static final String NODES = "nodes";

	public static final String LABELS = "labels";

	public static final String GROUPS = "groups";

	public static final String SOURCE = "source";

	public static final String TARGET = "target";

	public static final String GROUP = "group";

	public static final String LABEL = "label";

	public static final String PROP = "prop";

	public static final String URI = "uri";

}
