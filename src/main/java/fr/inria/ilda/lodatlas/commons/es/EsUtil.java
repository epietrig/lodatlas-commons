/*******************************************************************************
 * LODAtlas Commons
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.commons.es;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.NamedXContentRegistry;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentParser;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.discovery.MasterNotDiscoveredException;
import org.elasticsearch.index.mapper.MapperParsingException;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ilda.lodatlas.commons.es.exceptions.MappingFileException;

/**
 * 
 * 
 * @author Hande Gözükan
 *
 */
public class EsUtil {

	private static final Logger logger = LoggerFactory.getLogger(EsUtil.class);

	public static final String CLUSTER_NAME_KEY = "cluster.name";

	/**
	 * Number of trials to check Elasticsearch server.
	 */
	private static final int MAX_HEALTH_CHECK_COUNT = 5;

	private int healthCheck = 0;

	/**
	 * ElasticSearch client.
	 */
	private final TransportClient esClient;

	/**
	 * 
	 * @param clusterName
	 *            name of the elasticsearch cluster
	 * @param port
	 *            port for elasticsearch server
	 * @param host
	 *            host for elasticsearch server
	 */
	public EsUtil(String clusterName, int port, String host) {
		this.esClient = initEsClient(clusterName, port, host);
		checkEsHealth();
	}

	/**
	 * Initiates the elasticsearch client that will connect to cluster specified by
	 * <code>clusterName</code> on the specified <code>host</code> through the
	 * specified <code>port</code>.
	 * 
	 * @param clusterName
	 *            name of the elasticsearch cluster.
	 * @param port
	 *            port for elasticsearch server.
	 * @param host
	 *            host for elasticsearch server.
	 * @return the elasticsearch client.
	 */
	private TransportClient initEsClient(String clusterName, int port, String host) {
		TransportClient client = null;
		final Settings settings = Settings.builder().put(CLUSTER_NAME_KEY, clusterName).build();

		try {
			client = new PreBuiltTransportClient(settings);
			client.addTransportAddress(new TransportAddress(InetAddress.getByName(host), port));
		} catch (NumberFormatException e) {
			logger.error("Encountered number format exception while converting elastic search port number: " + port, e);

		} catch (UnknownHostException e) {
			logger.error("Encountered UnknownHostException for elastic search host: " + host, e);
		}

		return client;
	}

	/**
	 * Checks the health of the elastic search server.
	 * 
	 * After 5 trials, if health check is still not successful, exits the system.
	 * 
	 */
	private void checkEsHealth() {
		if (healthCheck > MAX_HEALTH_CHECK_COUNT)
			System.exit(1);

		healthCheck++;
		ClusterHealthResponse hr = null;
		try {
			hr = esClient.admin().cluster().prepareHealth().setWaitForGreenStatus()
					.setTimeout(TimeValue.timeValueMillis(10000)).execute().actionGet();
		} catch (MasterNotDiscoveredException e) {
			logger.error(
					"MasterNotDiscoveredException while checking health of elastic search server. " + e.getMessage(),
					e);
		} catch (Exception e) {
			logger.error("Exception while checking health of elastic search server. " + e.getMessage(), e);
		} finally {

			if (hr != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("Data nodes found:" + hr.getNumberOfDataNodes());
					logger.debug("Timeout? :" + hr.isTimedOut());
					logger.debug("Status:" + hr.getStatus().name());
				}
				healthCheck = 0;
			} else {
				if (logger.isWarnEnabled())
					logger.warn("Elasticsearch server health check failed. Check count " + healthCheck);
				try {
					Thread.currentThread();
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					if (logger.isWarnEnabled())
						logger.warn("InterruptedException while waiting to retry", e);
				}
				checkEsHealth();
			}
		}
	}

	/**
	 * Creates an Elasticsearch index with name <code>indexName</code> and with
	 * mapping type <code>mappingType</code> and corresponding mapping file
	 * specified with file name <code>mappingFile</code>.
	 * 
	 * @param indexName
	 *            name of the index to create.
	 * @param mappingType
	 *            mapping type of the index. This is necessary for Elasticsearch
	 *            version 6.x and below. Starting from Elasticsearch 6.x only one
	 *            mapping type can be specified for an index.
	 * @param mappingFile
	 *            mapping file name for <code>mappingType</code> located at
	 *            resources path. This is optional. If specified, the fields of the
	 *            documents to index will be indexed depending on the mappings in
	 *            the file. If set to <code>null</code> or empty string, then all
	 *            fields will be indexed but not stored.
	 * @param analyzersFile
	 *            file that specifies the index analyzers to be used. This is
	 *            necessary if custom analyzers are used in the
	 *            <code>mappingFile</code>.
	 * @return <code>true</code> if the index is created successfully;
	 *         <code>false</code> otherwise.
	 * @throws FileNotFoundException
	 *             if either the specified <code>mappingFile</code> or
	 *             <code>analyzersFile</code> is missing on the classpath.
	 * @throws MappingFileException
	 *             if the specified <code>mappingFile</code> has problems.
	 * @throws IllegalArgumentException
	 *             <ul>
	 *             <li>if the specified <code>indexName</code> is <code>null</code>
	 *             or empty string.</li>
	 *             <li>if the specified <code>mappingType</code> is
	 *             <code>null</code> or empty string.</li>
	 *             </ul>
	 */
	public boolean createIndex(String indexName, String mappingType, String mappingFile, String analyzersFile,
			boolean deleteIfExists) throws FileNotFoundException, MappingFileException {

		if (indexName == null || "".equals(indexName)) {
			throw new IllegalArgumentException("Index name cannot be null or empty string.");
		}

		if (mappingType == null || "".equals(mappingType)) {
			throw new IllegalArgumentException("Mapping type cannot be null or empty string.");
		}

		IndicesExistsResponse response = esClient.admin().indices().prepareExists(indexName).execute().actionGet();
		if (response.isExists()) {
			System.out.println("already exists");
			if (deleteIfExists) {
				esClient.admin().indices().prepareDelete(indexName).execute().actionGet();
			} else
				return true;
		}

		CreateIndexRequest cir = new CreateIndexRequest(indexName);
		if (mappingFile != null && !"".equals(mappingFile)) {
			try {
				cir.mapping(mappingType, getBuilderFromResourcePath(mappingFile));
				System.out.println("loaded mapping file");
			} catch (FileNotFoundException e) {
				throw new FileNotFoundException("Mapping file does not exist on classpath.");
			}
		}

		if (analyzersFile != null && !"".equals(analyzersFile)) {
			try {
				cir.settings(getBuilderFromResourcePath(analyzersFile));
				System.out.println("loaded analyzers file");
			} catch (FileNotFoundException e) {
				throw new FileNotFoundException("Analyzers file does not exist on classpath.");
			}
		}

		CreateIndexResponse createIndexResponse = null;

		try {
			createIndexResponse = esClient.admin().indices().create(cir).actionGet();
		} catch (MapperParsingException e) {
			throw new MappingFileException(e);
		}

		if (!createIndexResponse.isAcknowledged()) {
			if (logger.isWarnEnabled())
				logger.warn("Encountered problem while creating index " + indexName);
			return false;
		} else {

			if (logger.isDebugEnabled())
				logger.debug("Index " + indexName + " is created.");
			return true;
		}
	}

	/**
	 * Creates Elasticsearch index for the specified <code>json</code> in the index
	 * with specified <code>indexName</code>. <br>
	 * If the index with specified <code>indexName</code> does not exist, creates
	 * it.<br>
	 * 
	 * Assumes that the index and the type already exists in Elasticssearch and the
	 * document type corresponds to the specified type.
	 * 
	 * @param indexName
	 *            name of the index to store the created document index.
	 * @param mappingType
	 *            mapping type of the index.
	 * @param id
	 *            id for the document. This is optional. If no id is specified
	 *            Elasticsearch will create one.
	 * @param json
	 *            document to index.
	 * @return the response of the index operation.
	 * 
	 * @throws IllegalArgumentException
	 *             if
	 *             <ul>
	 *             <li>if the specified <code>indexName</code> is <code>null</code>
	 *             or empty string.</li>
	 *             <li>if the specified <code>mappingType</code> is
	 *             <code>null</code> or empty string.</li>
	 *             <li>if the specified <code>json</code> is <code>null</code> or
	 *             empty string.</li>
	 *             </ul>
	 */
	public IndexResponse indexDocument(String indexName, String mappingType, String id, String json) {
		if (indexName == null || "".equals(indexName)) {
			throw new IllegalArgumentException("Index name cannot be null or empty string.");
		}

		if (mappingType == null || "".equals(mappingType)) {
			throw new IllegalArgumentException("Mapping type cannot be null or empty string.");
		}

		if (json == null || "".equals(json)) {
			throw new IllegalArgumentException("Document to index cannot be null or empty string.");
		}

		return esClient.prepareIndex(indexName, mappingType, id).setSource(json, XContentType.JSON).get();
	}

	// TODO update document index

	// TODO delete document

	// TODO search

	// TODO refresh index

	// public SearchResponse searchIndex() {
	//
	// }

	/**
	 * Releases the resources.
	 */
	public void dispose() {
		esClient.close();
	}

	/**
	 * Loads the file specified by <code>fileName</code> in the classpath and
	 * returns the contents as {@link XContentBuilder} instance.
	 * 
	 * @param fileName
	 *            name of the file in classpath.
	 * 
	 *            return content of the file loaded to a {@link XContentBuilder}
	 *            instance.
	 * 
	 * @throws FileNotFoundException
	 *             if the file with <code>fileName</code> cannot be found in the
	 *             classpath.
	 */
	public static XContentBuilder getBuilderFromResourcePath(String fileName) throws FileNotFoundException {

		InputStream stream = ClassLoader.getSystemResourceAsStream(fileName);

		if (stream == null) {
			throw new FileNotFoundException();
		}

		XContentParser parser = null;
		try {
			parser = XContentFactory.xContent(XContentType.JSON).createParser(NamedXContentRegistry.EMPTY, stream);
		} catch (IOException e) {
			logger.error("IOException while reading file " + fileName, e);
		}

		XContentBuilder x = null;
		try {
			x = XContentFactory.jsonBuilder().copyCurrentStructure(parser);
		} catch (IOException e) {
			logger.error("IOException while reading file " + fileName, e);
		}

		return x;
	}

}
