package fr.inria.ilda.lodatlas.commons.voidprocess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RiotException;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.VOID;
import org.apache.log4j.Logger;

import fr.inria.ilda.lodatlas.commons.model.FormatType;
import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.commons.strings.ResourceStrings;

/**
 * 
 * @author Hande Gözükan
 *
 */
public class VoidParser {

	private static final Logger logger = Logger.getLogger(VoidParser.class);

	private static final String BASE_URI = "http://lodatlas.lri.fr";

	public VoidParser() {
	}

	/**
	 * Processes the VoID file specified by <code>uri</code> for the dataset
	 * specified by <code>datasetName</code> and generates and returns a JSON object
	 * from extracted fields.
	 * 
	 * @param datasetName
	 *            name of the dataset for the specified VoID file.
	 * @param uri
	 *            URI for the VoID file of the dataset.
	 * @return a JsonObject generated from the fields extracted from the VoID file.
	 */
	public JsonObject processDatasetVoid(String uri) {
		// load VoID file
		Model model = readModel(uri);

		if (model == null) {
			return null;
		}
		// TODO throw exception is the model cannot be read

		JsonObjectBuilder datasetBuilder = Json.createObjectBuilder();

		// check void:DatasetDescription to get main dataset
		RDFNode mainDataset = processDatasetDesc(model);

		// process linksets
		Map<String, Resource> linksets = new HashMap<>();

		List<Resource> datasets = new ArrayList<>();

		RDFNode mainDatasetFromLinksets = processLinksets(model, linksets, datasetBuilder);

		// check if main dataset from void:DatasetDescription and linkset
		// void:subjectsTarget are the same.
		if (mainDataset == null) {
			mainDataset = mainDatasetFromLinksets;
		} else if (mainDataset != mainDatasetFromLinksets) {
			// TODO
			logger.warn("Different datasets");
		}
		mainDataset = processDatasets(model, datasets, linksets, mainDataset);

		if (mainDataset != null) {
			processMainDataset(mainDataset, datasetBuilder);
		}

		model.close();

		return datasetBuilder.build();
	}

	/**
	 * 
	 * @param uri
	 * @return
	 */
	public Model readModel(String uri) {
		Model model = ModelFactory.createDefaultModel();

		// TODO throw exceptions

		try {
			logger.info("Reading from URI " + uri);
			RDFDataMgr.read(model, uri, BASE_URI, null);
		} catch (RiotException e) {
			logger.warn("Specified URI is not valid " + uri, e);
			return null;
		} catch (IllegalArgumentException e) {
			logger.warn("Specified URI is not valid " + uri, e);
			return null;
		} catch (Exception e) {
			logger.warn("Specified URI is not valid " + uri, e);
			return null;
		}
		return model;
	}

	/**
	 * Processes void:DatasetDescription section of the VoID file if it exists.
	 * Checks for foaf:primaryTopic property to identify the main dataset of the
	 * VoID file specified by the <code>model</code>.
	 * 
	 * @param model
	 *            model for the VoID file.
	 * @return the main dataset as RDFNode if it exists; <code>null</code>
	 *         otherwise.
	 */
	public RDFNode processDatasetDesc(Model model) {
		logger.info("Processing dataset descriptions");
		StmtIterator iterator = model.listStatements(null, RDF.type, VOID.DatasetDescription);

		logger.info("There exists " + (iterator.hasNext() ? "" : "no ") + "dataset description field");

		while (iterator.hasNext()) {
			Statement s = iterator.next().getSubject().getProperty(FOAF.primaryTopic);
			if (s != null) {
				return s.getObject();
			}
		}
		return null;
	}

	/**
	 * Processes void:Linkset sections of the VoID file specified by the
	 * <code>model</code>, sets the void:subjectsTarget as main dataset, and puts
	 * the void:objectsTarget as links in the specified <code>linkset</code> map.
	 * 
	 * 
	 * TODO triple counts are missing
	 * 
	 * @param model
	 *            RDF model that contains VoID file to check for linksets.
	 * @param linkset
	 *            the map to put the linkset URI as string and void:objectsTarget as
	 *            Resource.
	 * @return the main dataset as RDFNode.
	 */
	public RDFNode processLinksets(Model model, Map<String, Resource> linkset, JsonObjectBuilder datasetBuilder) {
		RDFNode mainDataset = null;

		logger.info("Processing linksets");
		StmtIterator iterator = model.listStatements(null, RDF.type, VOID.Linkset);

		logger.info("There exists " + (iterator.hasNext() ? "" : "no ") + "linksets");

		JsonObjectBuilder outgoingLinksBuilder = Json.createObjectBuilder();
		JsonArrayBuilder linksArray = Json.createArrayBuilder();
		int totalCount = 0;

		if (iterator.hasNext()) {

			// subject of the linkset
			Resource subject = null;

			while (iterator.hasNext()) {
				subject = iterator.next().getSubject();

				RDFNode subjectsTarget = subject.getProperty(VOID.subjectsTarget).getObject();
				RDFNode objectsTarget = subject.getProperty(VOID.objectsTarget).getObject();

				// set links
				if (objectsTarget != null) {
					logger.info("Adding link " + subject.toString() + " with target "
							+ objectsTarget.asResource().toString());

					JsonObjectBuilder linkBuilder = Json.createObjectBuilder();

					linkset.put(subject.asResource().toString(), objectsTarget.asResource());

					// add name field for the link
					linkBuilder.add(DatasetStrings.NAME, objectsTarget.asResource().toString());

					// add homepage specified for the objects target.
					// this field does not exist among the fields obtained from CKAN metadata. It is
					// added to be used to gather and process VoID files for the linksets.
					addResourceAsString(objectsTarget, FOAF.homepage, DatasetStrings.VOID, linkBuilder);

					// add count field for the link
					Statement triples = subject.getProperty(VOID.triples);
					if (triples != null) {
						linkBuilder.add(DatasetStrings.COUNT, triples.getLong());
					}

					linksArray.add(linkBuilder);
					totalCount++;
				} else {
					logger.warn("No object target is defined for the link " + subject.toString());
				}

				// set main dataset on the assumption that subjectsTarget should be the same for
				// all Linksets and it should be equal the dataset defined in this VoID file
				if (subjectsTarget != null) {
					if (mainDataset == null) {
						mainDataset = subjectsTarget;
						logger.info("Main dataset is " + mainDataset.toString());
					} else {
						if (!mainDataset.toString().equals(subjectsTarget.toString())) {
							logger.warn("There is a different subjects target!");
						} else {
							logger.info("Subjects target is equal to main dataset");
						}
					}
				}
			}
		}

		// add count field to the dataset outgoing links
		outgoingLinksBuilder.add(DatasetStrings.COUNT, totalCount);
		// add links array to the dataset outgoing links
		outgoingLinksBuilder.add(DatasetStrings.LINKS, linksArray);
		// add outgoing links to the dataset
		datasetBuilder.add(DatasetStrings.OUTGOING_LINKS, outgoingLinksBuilder);
		return mainDataset;
	}

	public RDFNode processDatasets(Model model, List<Resource> datasets, Map<String, Resource> linksets,
			RDFNode mainDataset) {

		// process datasets
		logger.info("Processing datasets");
		StmtIterator iterator = model.listStatements(null, RDF.type, VOID.Dataset);

		logger.info("There exists " + (iterator.hasNext() ? "" : "no ") + "datasets");

		Resource currentDataset = null;

		while (iterator.hasNext()) {
			currentDataset = iterator.next().getSubject();
			if (mainDataset != null) {
				if (!mainDataset.toString().equals(currentDataset.toString())) {
					logger.info("Adding " + currentDataset.toString() + " as other dataset.");

					datasets.add(currentDataset);
				} else {
					logger.info("The dataset is equal to main dataset, ignoring.");
				}
			} else {
				logger.info("No main dataset found from linksets or description.");
				datasets.add(currentDataset);
			}
		}

		if (mainDataset == null) {
			logger.info("There are " + datasets.size() + " other datasets.");
			if (datasets.size() == 1) {
				logger.info("Setting main dataset to " + datasets.get(0).toString());
				return datasets.get(0);
			} else {
				// TODO implement the case where more than one dataset is defined and the rest
				// is not linkset, how to find the main dataset
				logger.warn("Not implemented yet");
			}
		}

		return mainDataset;
	}

	/**
	 * Processes the specified <code>mainDataset</code>, extracts the necessary
	 * fields and sets them to JsonObjectBuilder.
	 * 
	 * @param mainDataset
	 * @param datasetBuilder
	 */
	public void processMainDataset(RDFNode mainDataset, JsonObjectBuilder datasetBuilder) {

		// get homepage (as url)
		addResourceAsString(mainDataset, FOAF.homepage, DatasetStrings.URL, datasetBuilder);

		// get title
		addLiteralAsString(mainDataset, DCTerms.title, DatasetStrings.TITLE, datasetBuilder);

		// get description
		addLiteralAsString(mainDataset, DCTerms.description, DatasetStrings.NOTES, datasetBuilder);

		// Get triple count
		RDFNode tripleCount = getObjectNode(mainDataset, VOID.triples);
		if (tripleCount != null) {
			logger.info("Triple count is " + tripleCount.asLiteral().getLong());
			datasetBuilder.add(DatasetStrings.TRIPLE_COUNT, tripleCount.asLiteral().getLong());
		}

		// get namespace
		datasetBuilder.add(DatasetStrings.NAMESPACE, mainDataset.asResource().getNameSpace());

		// get creator (as author)
		RDFNode creator = getObjectNode(mainDataset, DCTerms.creator);
		if (creator != null) {
			if (creator.isResource()) {
				// check if a type definition exists for this resource
				Statement defAuth = creator.asResource().getProperty(RDF.type);

				// if type def. does not exist, put URL as author.
				if (defAuth == null) {
					datasetBuilder.add(DatasetStrings.AUTHOR, creator.asResource().toString());
				} else {
					Resource authResource = defAuth.getObject().asResource();
					if (authResource.getProperty(RDFS.label) != null) {
						datasetBuilder.add(DatasetStrings.AUTHOR,
								authResource.getProperty(RDFS.label).getObject().toString());
					} else {
						datasetBuilder.add(DatasetStrings.AUTHOR, creator.asResource().toString());
					}

					if (authResource.getProperty(FOAF.mbox) != null) {
						datasetBuilder.add(DatasetStrings.AUTHOR_EMAIL,
								authResource.getProperty(FOAF.mbox).getObject().toString());
					}

					// TODO if label does not exist, check other fields that might be meaningful
				}

			} else {
				datasetBuilder.add(DatasetStrings.AUTHOR, creator.toString());
			}
		}

		// get publisher (as organization)
		RDFNode publisher = getObjectNode(mainDataset, DCTerms.publisher);
		if (publisher != null) {
			JsonObjectBuilder orgBuilder = Json.createObjectBuilder();

			if (publisher.isResource()) {
				// check if a type definition exists for this resource
				Statement defOrg = publisher.asResource().getProperty(RDF.type);

				// if type def. does not exist, put URL as title of org.
				if (defOrg == null) {
					orgBuilder.add(DatasetStrings.TITLE, publisher.asResource().toString());
				} else {
					Resource orgResource = defOrg.getObject().asResource();
					if (orgResource.getProperty(DCTerms.title) != null) {
						orgBuilder.add(DatasetStrings.TITLE,
								orgResource.getProperty(DCTerms.title).getObject().toString());
					} else if (orgResource.getProperty(RDFS.label) != null) {
						orgBuilder.add(DatasetStrings.TITLE,
								orgResource.getProperty(RDFS.label).getObject().toString());
					}

					// TODO if title does not exist check other fields
				}

			} else {
				orgBuilder.add(DatasetStrings.TITLE, publisher.toString());
			}

			datasetBuilder.add(DatasetStrings.ORGANIZATION, orgBuilder.build());
		}

		// get created; if created does not exist check for issued
		if (!addLiteralAsString(mainDataset, DCTerms.created, DatasetStrings.METADATA_CREATED, datasetBuilder)) {
			addLiteralAsString(mainDataset, DCTerms.issued, DatasetStrings.METADATA_CREATED, datasetBuilder);
		}

		// get modified
		addLiteralAsString(mainDataset, DCTerms.modified, DatasetStrings.METADATA_MODIFIED, datasetBuilder);

		// get license
		addResourceAsString(mainDataset, DCTerms.license, DatasetStrings.LICENSE_URL, datasetBuilder);

		// // get vocabularies
		// List<RDFNode> vocabularies = getObjectNodeList(mainDataset, VOID.vocabulary);
		// JsonArrayBuilder vocabulariesBuilder = null;
		// if (vocabularies != null) {
		// System.out.println(vocabularies.size());
		// vocabulariesBuilder = Json.createArrayBuilder();
		// for (RDFNode vocab : vocabularies) {
		// vocabulariesBuilder.add(vocab.asResource().toString());
		// logger.info("Vocabulary is " + vocab.asResource().toString());
		// }
		// }

		// get resources
		JsonArrayBuilder resourcesBuilder = Json.createArrayBuilder();

		// get sparql endpoint
		RDFNode sparqlResource = getObjectNode(mainDataset, VOID.sparqlEndpoint);
		if (sparqlResource != null) {
			JsonObjectBuilder resourceBuilder = Json.createObjectBuilder();
			resourceBuilder.add(ResourceStrings.FORMAT, FormatType.SPARQL.value());
			resourceBuilder.add(ResourceStrings.URL, sparqlResource.asResource().toString());
			logger.info("Sparql endpoint is " + sparqlResource.asResource().toString());
			resourcesBuilder.add(resourceBuilder);
		}

		// get exampleDatasets
		RDFNode exampleResource = getObjectNode(mainDataset, VOID.exampleResource);
		if (exampleResource != null) {
			// TODO check if resource has definition
			JsonObjectBuilder resourceBuilder = Json.createObjectBuilder();
			resourceBuilder.add(ResourceStrings.URL, exampleResource.asResource().toString());
			logger.info("Example resource is " + exampleResource.asResource().toString());
			resourcesBuilder.add(resourceBuilder);
		}

		// get dataDumps
		List<RDFNode> dataDumps = getObjectNodeList(mainDataset, VOID.dataDump);
		if (dataDumps != null) {
			for (RDFNode dumpResource : dataDumps) {
				// TODO check if resource has definition
				JsonObjectBuilder resourceBuilder = Json.createObjectBuilder();
				resourceBuilder.add(ResourceStrings.URL, dumpResource.asResource().toString());
				// // add vocabularies
				// if (vocabulariesBuilder != null) {
				// resourceBuilder.add(ProcessedFileStrings.VOCABULARIES, vocabulariesBuilder);
				// }
				logger.info("Dump resource is " + dumpResource.asResource().toString());
				resourcesBuilder.add(resourceBuilder);
			}
		}

		datasetBuilder.add(DatasetStrings.RESOURCES, resourcesBuilder);

		// get subject as tag
		List<RDFNode> subjects = getObjectNodeList(mainDataset, DCTerms.subject);

		if (subjects != null) {
			JsonArrayBuilder tagsBuilder = Json.createArrayBuilder();
			for (RDFNode subject : subjects) {
				tagsBuilder.add(subject.asResource().toString().replaceAll("http://dbpedia.org/resource/", ""));
				logger.info("Dump resource is " + subject.asResource().toString());
			}
			datasetBuilder.add(DatasetStrings.TAGS, tagsBuilder);
		}

	}

	private Resource addResourceAsString(RDFNode node, Property property, String fieldName, JsonObjectBuilder jsonObj) {

		RDFNode objectNode = getObjectNode(node, property);

		if (objectNode != null) {
			logger.info("Field value is " + objectNode.toString());
			jsonObj.add(fieldName, objectNode.asResource().toString());
			return objectNode.asResource();
		}
		return null;
	}

	private boolean addLiteralAsString(RDFNode node, Property property, String fieldName, JsonObjectBuilder jsonObj) {

		RDFNode objectNode = getObjectNode(node, property);

		if (objectNode != null) {
			logger.info("Field value is " + objectNode.toString());
			jsonObj.add(fieldName, objectNode.asLiteral().getLexicalForm().toString());
			return true;
		}
		return false;
	}

	/**
	 * Getter method for the object node of the specified <code>property</code> of
	 * the specified <code>node</code> if the specified <code>property</code>
	 * exists.
	 * 
	 * @param node
	 *            the node to check for the object node for the specified
	 *            <code>property</code>.
	 * @param property
	 *            the property whose object node is required.
	 * @return the object node of the specified <code>node</code> for the specified
	 *         <code>property</code> if it exists; <code>null</code> otherwise.
	 */
	private RDFNode getObjectNode(RDFNode node, Property property) {
		Statement stmt = node.asResource().getProperty(property);

		if (stmt != null) {
			return stmt.getObject();
		}
		return null;
	}

	/**
	 * 
	 * @param mainDataset
	 * @param property
	 * @return
	 */
	private List<RDFNode> getObjectNodeList(RDFNode mainDataset, Property property) {
		StmtIterator stmts = mainDataset.asResource().listProperties(property);

		List<RDFNode> nodes = null;

		if (stmts.hasNext()) {
			nodes = new ArrayList<>();

			while (stmts.hasNext()) {
				nodes.add(stmts.next().getObject());
			}
			return nodes;
		}
		return null;
	}

}
