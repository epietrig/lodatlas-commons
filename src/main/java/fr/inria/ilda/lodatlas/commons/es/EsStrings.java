/*******************************************************************************
 * LODAtlas Commons
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.commons.es;

/**
 * A class that lists Elasticsearch configuration file parameter names.
 * 
 * @author Hande Gözükan
 *
 */
public class EsStrings {

	public static final String HOST = "esHost";

	public static final String PORT = "esPort";

	public static final String CLUSTER = "cluster";

	public static final String ANALYZERS_FILE = "analyzersFile";

	public static final String DATASET_INDEX = "datasetIndex";

	public static final String DATASET_MAPPING = "datasetMapping";

	public static final String DATASET_TYPE = "datasetType";

	public static final String PROCESSED_FILE_INDEX = "processedFileIndex";

	public static final String PROCESSED_FILE_MAPPING = "processedFileMapping";

	public static final String PROCESSED_FILE_TYPE = "processedFileType";

	public static final String VOCABULARY_INDEX = "vocabularyIndex";

	public static final String VOCABULARY_MAPPING = "vocabularyMapping";

	public static final String VOCABULARY_TYPE = "vocabularyType";

}