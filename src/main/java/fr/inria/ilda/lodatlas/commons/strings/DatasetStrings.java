/*******************************************************************************
 * LODAtlas Commons
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.commons.strings;

/**
 * 
 * 
 * @author Hande Gözükan
 *
 */
public class DatasetStrings {
	
	public static final String ID = "id";

	/**
	 * Field name for dataset name, incoming/outgoing link name.
	 */
	public static final String NAME = "name";

	/**
	 * Field name for repository name.
	 */
	public static final String REPO_NAME = "repoName";

	/**
	 * Field name for dataset URL.
	 */
	public static final String URL = "url";

	/**
	 * URL of the dataset in the repository it belongs to
	 */
	public static final String REPO_DATASET_URL = "repoDatasetUrl";

	public static final String NOTES = "notes";

	public static final String TITLE = "title";

	public static final String AUTHOR = "author";

	public static final String AUTHOR_EMAIL = "author_email";

	public static final String MAINTAINER = "maintainer";

	public static final String MAINTAINER_EMAIL = "maintainer_email";

	public static final String NAMESPACE = "namespace";

	/**
	 * Field name for triple count.
	 */
	public static final String TRIPLE_COUNT = "tripleCount";

	public static final String METADATA_CREATED = "metadata_created";

	public static final String METADATA_MODIFIED = "metadata_modified";

	public static final String LICENSE_URL = "license_url";

	public static final String LICENSE_TITLE = "license_title";

	public static final String STATE = "state";

	public static final String VERSION = "version";

	public static final String ORGANIZATION = "organization";

	public static final String DESCRIPTION = "description";

	public static final String TAGS = "tags";

	/**
	 * Field name for resources list.
	 */
	public static final String RESOURCES = "resources";

	/**
	 * Field name for incoming links.
	 */
	public static final String INCOMING_LINKS = "incominglinks";

	/**
	 * Field name for outgoing links.
	 */
	public static final String OUTGOING_LINKS = "outgoinglinks";

	/**
	 * Field name for link count field.
	 */
	public static final String COUNT = "count";
	
	/**
	 * Field name for VoID URL field.
	 */
	public static final String VOID = "voidurl";

	/**
	 * Field name for links array.
	 */
	public static final String LINKS = "links";

	public static final String KEY = "key";

	public static final String VALUE = "value";

	public static final String EXTRAS = "extras";

	public static final String TRIPLES = "triples";

}
