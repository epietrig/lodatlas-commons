/*******************************************************************************
 * LODAtlas Datamanager
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.commons.model;

import java.util.HashSet;
import java.util.Set;

import javax.json.bind.annotation.JsonbTransient;

public class Vocabulary {

	private String id = null;

	private String uri = "";

	private String lovUri = "";

	private String title = "-";

	private String prefix = "";

	private String homepage = "";

	private String description = "";

	private String error = "";

	private Set<Label> classes = new HashSet<>();

	private Set<Label> properties = new HashSet<>();

	public Vocabulary() {
	}

	public Vocabulary(String uri, String title, String prefix, String homepage, String description, String error) {

		this.uri = uri;
		this.lovUri = !"".equals(prefix) ? "http://lov.okfn.org/dataset/lov/vocabs/" + prefix : prefix;
		this.title = title;
		this.prefix = prefix;
		this.homepage = homepage;
		this.description = description;
		this.error = error;
	}

	@JsonbTransient
	public String getId() {
		if (this.id == null)
			return null;
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getLovUri() {
		return lovUri;
	}

	public void setLovUri(String lovUri) {
		this.lovUri = lovUri;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public void addClass(Label classLabel) {
		this.classes.add(classLabel);
	}

	public Set<Label> getClasses() {
		return classes;
	}

	public void setClasses(Set<Label> classes) {
		this.classes = classes;
	}

	public void addProperty(Label propertyLabel) {
		this.properties.add(propertyLabel);
	}

	public Set<Label> getProperties() {
		return properties;
	}

	public void setProperties(Set<Label> properties) {
		this.properties = properties;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classes == null) ? 0 : classes.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((error == null) ? 0 : error.hashCode());
		result = prime * result + ((homepage == null) ? 0 : homepage.hashCode());
		result = prime * result + ((prefix == null) ? 0 : prefix.hashCode());
		result = prime * result + ((properties == null) ? 0 : properties.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		result = prime * result + ((lovUri == null) ? 0 : lovUri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vocabulary other = (Vocabulary) obj;
		if (classes == null) {
			if (other.classes != null)
				return false;
		} else if (!classes.equals(other.classes))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (error == null) {
			if (other.error != null)
				return false;
		} else if (!error.equals(other.error))
			return false;
		if (homepage == null) {
			if (other.homepage != null)
				return false;
		} else if (!homepage.equals(other.homepage))
			return false;
		if (prefix == null) {
			if (other.prefix != null)
				return false;
		} else if (!prefix.equals(other.prefix))
			return false;
		if (properties == null) {
			if (other.properties != null)
				return false;
		} else if (!properties.equals(other.properties))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		if (lovUri == null) {
			if (other.lovUri != null)
				return false;
		} else if (!lovUri.equals(other.lovUri))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vocabulary [id=" + id + ", uri=" + uri + ", lovUri=" + lovUri + ", title=" + title + ", prefix="
				+ prefix + ", homepage=" + homepage + ", description=" + description + ", error=" + error + ", classes="
				+ classes + ", properties=" + properties + "]";
	}

}
