/*******************************************************************************
 * LODAtlas Commons
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.commons.model;

/**
 * An enumeration class to list possible format types for a dataset resource
 * {@link IResource}. <br>
 * It identifies the format types which can be classified as linked data and
 * also identifies the format types which can be downloaded for further
 * processing.
 * 
 * @see IResource
 * @author Hande Gözükan
 *
 */
public enum FormatType {

	SPARQL("sparql"), RDF("rdf"), TTL("ttl"), N3("n3"), NT("nt"), NQ("nq"), TRIG("trig"), LD_JSON("ld-json"), TRIX(
			"trix"), META("meta"), VOID("void"), OWL("owl"), RDFS("rdfs"), DCAT(
					"dcat"), RDFA("rdfa"), LINKED_DATA("linked-data"), HDT("hdt"), OTHER("other"), NONE("");

	private final String format;

	private FormatType(String format) {
		this.format = format;
	}

	public String value() {
		return format;
	}

	/**
	 * Returns boolean value to indicate if this format can be downloaded for data
	 * processing.
	 * 
	 * @return <code>true</code> if this format can be downloaded for processing;
	 *         <code>false</code> otherwise.
	 */
	public boolean canDownload() {
		if (this.equals(RDF) || this.equals(TTL) || this.equals(N3) || this.equals(NT) || this.equals(NQ)
				|| this.equals(TRIG) || this.equals(LD_JSON) || this.equals(TRIX) || this.equals(RDFA)
				|| this.equals(LINKED_DATA) || this.equals(NONE)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns boolean value to indicate if this format is linked data format.
	 * 
	 * @return <code>true</code> if this format is linked data format;
	 *         <code>false</code> otherwise.
	 */
	public boolean isLinkedDataFormat() {
		if (!this.equals(OTHER)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the most matching {@link FormatType} corresponding the specified
	 * <code>format</code> string.
	 * 
	 * @param format
	 *            format as string which might not match exactly to the strings
	 *            value of the {@link FormatType}s defined. It will check for the
	 *            closest match.
	 * @return the most matching {@link FormatType} corresponding the specified
	 *         <code>format</code> string.
	 */
	public static FormatType getFormat(String format) {
		if (format == null || "".equals(format)) {
			return FormatType.NONE;
		} else if (format.matches("(?i:.*sparq.*)") || format.matches("(?i:.*endpo.*)")) {
			return FormatType.SPARQL;
		} else if (format.matches("(?i:.*turt.*)") || format.matches("(?i:.*ttl.*)")) {
			return FormatType.TTL;
		} else if (format.matches("(?i:.*triple.*)")) {
			return FormatType.NT;
		} else if (format.matches("(?i:.*quads.*)") || format.matches("(?i:.*nq.*)")) {
			return FormatType.NQ;
		} else if (format.matches("(?i:.*trig.*)")) {
			return FormatType.TRIG;
		} else if (format.matches("(?i:.*n3.*)")) {
			return FormatType.N3;
		} else if (format.matches("(?i:.*ld.*)")) {
			return FormatType.LD_JSON;
		} else if (format.matches("(?i:.*trix.*)")) {
			return FormatType.TRIX;
		} else if (format.matches("(?i:.*meta.*)")) {
			return FormatType.META;
		} else if (format.matches("(?i:.*void.*)")) {
			return FormatType.VOID;
		} else if (format.matches("(?i:.*owl.*)")) {
			return FormatType.OWL;
		} else if (format.matches("(?i:.*rdfs.*)")) {
			return FormatType.RDFS;
		} else if (format.matches("(?i:.*dcat.*)")) {
			return FormatType.DCAT;
		} else if (format.matches("(?i:.*rdfa.*)")) {
			return FormatType.RDFA;
		} else if (format.matches("(?i:.*linked.*)")) {
			return FormatType.LINKED_DATA;
		} else if (format.matches("(?i:.*hdt.*)")) {
			return FormatType.HDT;
		} else if (format.contains("nt.") || format.contains(":nt") || format.equals("nt") || format.equals("rdf/nt")
				|| format.equals("rdf, nt")) {
			return FormatType.NT;
		} else if (format.matches("(?i:.*rdf.*)")) {
			return FormatType.RDF;
		} else if (format.matches("(?i:.*csv.*)") || format.matches("(?i:.*txt.*)") || format.matches("(?i:.*xls.*)")
				|| format.matches("(?i:.*excel.*)") || format.matches("(?i:.*pdf.*)")
				|| format.matches("(?i:.*jpeg.*)") || format.matches("(?i:.*shp.*)") || format.matches("(?i:.*ods.*)")
				|| format.matches("(?i:.*aspx.*)") || format.matches("(?i:.*png.*)") || format.matches("(?i:.*bin.*)")
				|| format.matches("(?i:.*url.*)") || format.matches("(?i:.*tsv.*)") || format.matches("(?i:.*stata.*)")
				|| format.matches("(?i:.*doc.*)") || format.matches("(?i:.*search.*)")) {
			return FormatType.OTHER;
		} else {
			// for such kind of resources, we try to download and decide format depending on
			// dump's content-type
			return FormatType.NONE;
		}
	}

	/**
	 * Returns the most matching {@link FormatType} corresponding the specified
	 * <code>mimeType</code> string.
	 * 
	 * @param mimeType
	 *            mimeType of to convert to format.
	 * @return he most matching {@link FormatType} corresponding the specified
	 *         <code>format</code> string.
	 */
	public static FormatType getFormatByMimeType(String mimeType) {
		if (mimeType == null || "".equals(mimeType)) {
			return FormatType.NONE;
		} else if (mimeType.startsWith("text/html") || mimeType.startsWith("application/json")
				|| mimeType.startsWith("text/xml")) {
			return FormatType.OTHER;
		} else if (mimeType.startsWith("application/n-triples")) {
			return FormatType.NT;
		} else if (mimeType.startsWith("text/turtle")) {
			return FormatType.TTL;
		} else if (mimeType.startsWith("application/rdf+xml")) {
			return FormatType.RDF;
		} else if (mimeType.startsWith("application/n-quads")) {
			return FormatType.NQ;
		} else if (mimeType.startsWith("text/n3")) {
			return FormatType.N3;
		} else if (mimeType.startsWith("application/ld+json")) {
			return FormatType.LD_JSON;
		} else if (mimeType.startsWith("application/trig")) {
			return FormatType.TRIG;
		} else if (mimeType.startsWith("application/trix")) {
			return FormatType.TRIX;
		} else {
			return FormatType.NONE;
		}
	}

}
